export const environment = {
  production: true,
  firebaseConfig: {
    apiKey: "AIzaSyC2s9f8NDxFvxdg14RRlFHQ99zCh7OW4MQ",
    authDomain: "mock-trello.firebaseapp.com",
    databaseURL: "https://mock-trello.firebaseio.com",
    projectId: "mock-trello",
    storageBucket: "mock-trello.appspot.com",
    messagingSenderId: "455854449034",
    appId: "1:455854449034:web:a4bc77c541056e0d822873",
    measurementId: "G-SRK4F4TBC1"
  }
};
