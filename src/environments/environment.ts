// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyC2s9f8NDxFvxdg14RRlFHQ99zCh7OW4MQ",
    authDomain: "mock-trello.firebaseapp.com",
    databaseURL: "https://mock-trello.firebaseio.com",
    projectId: "mock-trello",
    storageBucket: "mock-trello.appspot.com",
    messagingSenderId: "455854449034",
    appId: "1:455854449034:web:a4bc77c541056e0d822873",
    measurementId: "G-SRK4F4TBC1"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
