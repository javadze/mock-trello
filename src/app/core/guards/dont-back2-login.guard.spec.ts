import { TestBed } from '@angular/core/testing';

import { DontBack2LoginGuard } from './dont-back2-login.guard';

describe('DontBack2LoginGuard', () => {
  let guard: DontBack2LoginGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(DontBack2LoginGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
