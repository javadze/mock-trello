import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthStateService } from '../services/auth-state.service';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DontBack2LoginGuard implements CanActivate {
  constructor(private authState: AuthStateService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return this.authState.isLoggedIn().pipe(map(isLoggedIn => {
      if (isLoggedIn) {
         this.router.navigate(['/todos']);
      }
      return true;
    }));
  }


}
