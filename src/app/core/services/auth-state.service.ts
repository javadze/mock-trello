import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class AuthStateService {

  constructor(private afAuth: AngularFireAuth) { }


  isLoggedIn(): Observable<boolean> {
     return this.afAuth.user.pipe(map(user => !!user))
  }
}
