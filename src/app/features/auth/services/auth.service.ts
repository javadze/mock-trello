import { Injectable } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import * as firebase from 'firebase/app';
import {ToastrService} from 'ngx-toastr'
import { Router } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private afAuth: AngularFireAuth, private toaster: ToastrService, private router: Router) { }


  async login(email: string, password: string) {
    try {
      await this.afAuth.signInWithEmailAndPassword(email, password);
      this.toaster.success('you successfully loggedIn first auth', 'successful');
      this.goToProjectsPage();
    } catch (e) {
      this.toaster.error(e.message, e.name);

    }
  }

  async signUp(email: string, password: string) {

    try {
      await this.afAuth.createUserWithEmailAndPassword(email, password);
      this.toaster.success('thanks for signing up', 'sign up');
      this.goToProjectsPage();
    } catch (e) {
      this.toaster.error(e.message, e.name);

    }
  }

  goToProjectsPage() {
    this.router.navigate(['todos/projects']);
  }

}
