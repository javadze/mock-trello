import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('')
  });
  loader = false;
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

 async onSubmit() {
    const {email, password} = this.formGroup.getRawValue();

    this.loader = true;
    await this.authService.login(email, password);
    this.loader = false;

  }

}
