import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  formGroup = new FormGroup({
    email: new FormControl(''),
    password: new FormControl('', {validators: [Validators.minLength(6)]})
  });
  loader = false
  constructor(private authService: AuthService) { }

  ngOnInit(): void {
  }

 async onSubmit() {
    const {email, password} = this.formGroup.value;
    this.loader = true;
    await this.authService.signUp(email, password);
    this.loader = false;
  }

}
