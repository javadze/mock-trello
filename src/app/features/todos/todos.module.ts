import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';


import { TodoDialogComponent } from './components/todo-dialog/todo-dialog.component';
import { TodosProjectsComponent } from './pages/todos-projects/todos-projects.component';
import { TodosProjectContainerComponent } from './pages/todos-project-container/todos-project-container.component';
import { TodosListComponent } from './components/todo-list/todos-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { TodosRoutingModule } from './todos-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { MatIconModule } from '@angular/material/icon';


@NgModule({
  declarations: [
  TodosProjectsComponent,
  TodosListComponent,
  TodosProjectContainerComponent,
  TodosProjectContainerComponent
],
  imports: [
    CommonModule,
    DragDropModule,
    TodosRoutingModule,
    MatInputModule,
    ReactiveFormsModule,
    MatButtonModule,
    SharedModule,
    MatIconModule
  ],
  entryComponents: [
    TodoDialogComponent
  ]
})
export class TodosModule { }
