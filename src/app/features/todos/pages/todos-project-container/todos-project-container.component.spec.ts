import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosProjectContainerComponent } from './todos-project-container.component';

describe('TodosProjectContainerComponent', () => {
  let component: TodosProjectContainerComponent;
  let fixture: ComponentFixture<TodosProjectContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodosProjectContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosProjectContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
