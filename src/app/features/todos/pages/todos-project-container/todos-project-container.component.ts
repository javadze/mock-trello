import { Component, OnInit } from '@angular/core';
import { ITodoList } from '../../models/todo.model';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-todos-project-container',
  templateUrl: './todos-project-container.component.html',
  styleUrls: ['./todos-project-container.component.scss']
})
export class TodosProjectContainerComponent implements OnInit {
todoLists: ITodoList[];
addNewListForm = new FormControl('');
todoForm = new FormControl('');
  constructor() {
    this.todoLists = []
    for (let i =0; i < 8; i++) {
       this.todoLists.push({id:i.toString() , todoName: 'a' + i, todos: []})
    }
  }

  ngOnInit(): void {
  }


  addTodo(text: string, index: number) {
    this.todoLists[index].todos.push({text,color: null, description: ''});
     this.todoLists = [...this.todoLists]
  }
}
