import { Component, OnInit } from '@angular/core';
import { IProjects } from '../../models/projects.model';

@Component({
  selector: 'app-todos-projects',
  templateUrl: './todos-projects.component.html',
  styleUrls: ['./todos-projects.component.scss']
})
export class TodosProjectsComponent implements OnInit {
  projects: IProjects[];
  constructor() {
    this.projects = [
      {
        name: 'tester',
        image: 'https://image.shutterstock.com/image-photo/white-transparent-leaf-on-mirror-600w-1029171697.jpg',
        id: ''
      }
    ]
  }

  ngOnInit(): void {
  }

}
