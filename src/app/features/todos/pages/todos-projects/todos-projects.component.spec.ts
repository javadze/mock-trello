import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodosProjectsComponent } from './todos-projects.component';

describe('TodosProjectsComponent', () => {
  let component: TodosProjectsComponent;
  let fixture: ComponentFixture<TodosProjectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodosProjectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodosProjectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
