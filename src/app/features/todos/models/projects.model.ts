export interface IProjects {
  id?: string;
  name: string;
  readers?: string[];
  leaders?: string[];
  image: string;

}
