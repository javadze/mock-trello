export interface ITodoList {
  todoName: string;
  todos: ITodo[];
  id:string;
}


export interface ITodo {
  text: string;
  description: string;
  color: TodoColors;
  id?: string
}


export enum TodoColors {
  red = '#f94144',
  green = '#90be6d',
  yellow = '#f9c74f'
}
