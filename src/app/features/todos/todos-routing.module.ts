import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TodosProjectsComponent } from './pages/todos-projects/todos-projects.component';
import { TodosProjectContainerComponent } from './pages/todos-project-container/todos-project-container.component';


const routes: Routes = [
  { path: '',
    redirectTo: 'projects',
    pathMatch: 'full'
  },
  {
    path: 'projects',
    component: TodosProjectsComponent
  },
  {
    path: 'projects/todo-lists',
    component: TodosProjectContainerComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TodosRoutingModule { }
