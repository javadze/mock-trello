import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, Input, Output, HostListener, ElementRef, ViewChild, ViewChildren, QueryList } from '@angular/core';
import { ITodoList, ITodo } from '../../models/todo.model';
import { moveItemInArray, transferArrayItem, CdkDragDrop } from '@angular/cdk/drag-drop';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-todos-list',
  templateUrl: './todos-list.component.html',
  styleUrls: ['./todos-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodosListComponent implements OnInit {
  @Input() todoList: ITodoList;
  @Input() todoListConnected: ITodo[];
  todoForm = new FormControl('');
  @Output() addTodo = new EventEmitter<string>();
  wasInside = true;
  @ViewChildren('todoInp') todoInp: QueryList<ElementRef>;
  ngOnInit(): void {

  }
  @HostListener('click')
  clickInside() {
    this.wasInside = true;
  }

  @HostListener('document:click')
  clickout() {
    if (!this.wasInside) {
      this.todoForm.reset();
    }
    this.wasInside = false;
  }

  drop(event: CdkDragDrop<string[]>) {
    console.log(event);

    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);


    }
  }


  onAddTodo() {
     if(this.todoForm.valid) {
       this.addTodo.emit(this.todoForm.value);
       this.todoForm.reset();
     }
  }


}
