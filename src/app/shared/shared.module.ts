import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArrayFilterPipe } from './pipes/array-filter.pipe';



@NgModule({
  declarations: [
    ArrayFilterPipe
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ArrayFilterPipe
  ]

})
export class SharedModule { }
