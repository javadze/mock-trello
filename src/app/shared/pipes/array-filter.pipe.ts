import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'arrayFilter'
})
export class ArrayFilterPipe implements PipeTransform {

  transform(value: {[key: string]: any}[], fileterName: string): {[key: string]: any}[] {
    return value.map(item => item[fileterName]);
  }

}
